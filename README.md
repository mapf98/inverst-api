# API REST - Bitgobi Page Micro-service

## Empezando

### Prerrequisitos

- [Node.js](https://nodejs.org/es/)

### Instalar

1. Clona el repositorio

2. Crea un archivo .env tomando como ejemplo .env.example y añade tus credenciales

3. Instala las dependencias

```sh
npm install
```

### Inicia la API

```sh
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

### Recordatorio para pruebas

Ruta base: http://<host>/api/v1

# POR IMPLEMENTAR (REFERENCIAS)
- https://www.npmjs.com/package/vue-cookie-law
- https://www.npmjs.com/package/vue-cookie-accept-decline
- https://www.npmjs.com/package/vue-cookieconsent-component
- https://www.npmjs.com/package/nuxt-cookie-control
- https://cookie-script.com/pricing