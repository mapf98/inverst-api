import { Injectable } from '@nestjs/common';
import { APIStatus } from './entities/api.status.entity';

@Injectable()
export class AppService {
  getAPIStatus(): APIStatus {
    return new APIStatus('Management API active', 200, new Date());
  }
}
