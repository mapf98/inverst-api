import { ApiProperty } from '@nestjs/swagger'; // Import the necessary decorator

export class APIStatus {
  @ApiProperty({
    description: 'Status message',
    example: 'Management API active',
  })
  message: string;

  @ApiProperty({ description: 'HTTP status code', example: 200 })
  code: number;

  @ApiProperty({
    description: 'Date of the status',
    example: '2023-08-25T12:34:56Z',
  })
  date: Date;

  constructor(message: string, code: number, date: Date) {
    this.message = message;
    this.code = code;
    this.date = date;
  }
}
