import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Connection } from 'typeorm';
import { DatabaseModule } from '../common/database/database.module';
import { LoggerModule } from 'src/common/logger/logger.module';
import databaseConfiguration from '../common/config/database.configuration';
import configuration from '../common/config/configuration';
import { indexModules } from 'src/modules/index-modules';
import { LoggerService } from 'src/common/logger/logger.service';
import mailConfiguration from 'src/common/config/mail.configuration';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration, databaseConfiguration, mailConfiguration],
    }),
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        ttl: config.get('api.throttleTTL'),
        limit: config.get('api.throttleLimit'),
      }),
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../../', 'FILES/static'),
    }),
    DatabaseModule,
    LoggerModule,
    ...indexModules,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    LoggerService,
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
