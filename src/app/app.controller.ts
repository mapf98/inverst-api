import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { APIStatus } from './entities/api.status.entity';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'; // Import the necessary decorators

@Controller()
@ApiTags('API Status') // Add a tag to categorize your API in Swagger
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiOperation({ summary: 'Get API Status' }) // Describe the operation
  @ApiResponse({
    status: 200,
    description: 'API status retrieved successfully',
    type: APIStatus,
  })
  getAPIStatus(): APIStatus {
    return this.appService.getAPIStatus();
  }
}
