import { registerAs } from '@nestjs/config';

export default registerAs('api', () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  prefix: process.env.API_PREFIX,
  production: process.env.PRODUCTION,
  throttleTTL: process.env.THROTTLE_TTL,
  throttleLimit: process.env.THROTTLE_LIMIT,
  passwordSaltForHash: parseInt(process.env.PASSWORD_SALT_FOR_HASH),
  appHost: process.env.APP_HOST,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpiration: process.env.JWT_EXPIRATION,
  rmqHost: process.env.RMQ_HOST,
  twilioSID: process.env.ACCOUNT_SID,
  twilioToken: process.env.ACCOUNT_AUTH,
}));
