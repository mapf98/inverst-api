import { registerAs } from '@nestjs/config';

export default registerAs('mail', () => ({
  host: process.env.MAIL_HOST,
  user: process.env.MAIL_USER,
  password: process.env.MAIL_PASSWORD,
  transport: process.env.MAIL_TRANSPORT,
  defaultMail: process.env.DEFAULT_MAIL,
  template: {
    application: __dirname + './../mail/templates/application.template.hbs',
    application_not_elegible:
      __dirname + './../mail/templates/application-not-elegible.template.hbs',
    application_for_client:
      __dirname + './../mail/templates/application-for-client.template.hbs',
    opt: __dirname + './../mail/templates/opt.template.hbs',
  },
  subject: {
    application: 'INVERSTUCASA - Nueva cotización',
    application_not_elegible: 'INVERSTUCASA - Nueva cotización no elegible',
    application_for_client: 'INVERSTUCASA - Solicitud registrada',
    opt: 'INVERSTUCASA - Código de validación',
  },
}));
