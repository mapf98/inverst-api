import * as dotenv from 'dotenv';
dotenv.config();

export const ecpConfig = {
  key: process.env.ECP_TOKEN,
  algorithm: 'aes-256-gcm',
  ivLength: 16,
};
