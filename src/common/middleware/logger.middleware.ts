import { Logger } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

export function logger(req: Request, res: Response, next: NextFunction) {
  const logger = new Logger('HTTP');

  const { ip, method, originalUrl } = req;

  res.on('finish', () => {
    const { statusCode } = res;
    const contentLength = res.get('content-length');

    logger.log(
      `${method} ${originalUrl} ${statusCode} ${contentLength}kb - IP ${ip}`,
    );
  });

  next();
}
