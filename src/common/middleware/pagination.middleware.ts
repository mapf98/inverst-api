import { Injectable } from '@nestjs/common';
import { NestMiddleware } from '@nestjs/common';

@Injectable()
export class PaginationMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const limit = req.query.size ? +req.query.size : 100;
    const offset = req.query.page ? (req.query.page - 1) * limit : 0;

    req.pagination = { limit, offset };
    next();
  }
}
