import { IsArray, IsNotEmpty, IsObject, IsString } from 'class-validator';

export class MailDTO {
  @IsNotEmpty()
  @IsArray()
  to: Array<string>;

  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsNotEmpty()
  @IsString()
  template: string;

  @IsNotEmpty()
  @IsObject()
  context: unknown;

  @IsObject()
  attachment: { file: string; name: string };

  constructor(
    to: Array<string>,
    subject: string,
    template: string,
    context: unknown,
    attachment?: { file: string; name: string },
  ) {
    this.to = to;
    this.subject = subject;
    this.template = template;
    this.context = context;
    this.attachment = attachment;
  }
}
