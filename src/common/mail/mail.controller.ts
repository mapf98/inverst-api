import { MailService } from './mail.service';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { MailDTO } from 'src/common/mail/dto/mail.dto';
import { Controller } from '@nestjs/common';

@Controller()
export class MailController {
  constructor(private readonly mailService: MailService) {}

  @MessagePattern('application-email')
  async sendApplicationEmail(@Payload() data: MailDTO): Promise<void> {
    await this.mailService.sendApplicationEmail(data);
  }
}
