import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { LoggerService } from '../logger/logger.service';
import { MailDTO } from './dto/mail.dto';

@Injectable()
export class MailService {
  constructor(
    private mailerService: MailerService,
    private readonly logger: LoggerService,
    private readonly configService: ConfigService,
  ) {}

  private async sendEmail(mail: MailDTO) {
    const data: any = mail.context;

    Object.keys(data).forEach((key) => {
      if (data[key] == 'true') data[key] = true;
      if (data[key] == 'false') data[key] = false;
    });

    data.management_value = new Intl.NumberFormat('de-DE')
      .format(data.management_value)
      .toString();
    data.apt_value = new Intl.NumberFormat('de-DE')
      .format(data.apt_value)
      .toString();
    data.assessment_value = new Intl.NumberFormat('de-DE')
      .format(data.assessment_value)
      .toString();

    let attachments = [];
    if (mail.attachment)
      attachments = (mail.attachment as any).map((att) => {
        return {
          path: process.env.FILES_DIR + '/' + att.file,
          filename: att.name,
          contentDisposition: 'attachment',
        };
      });
    const mail_info: any = {
      from: this.configService.get('mail.user'),
      context: data,
      subject: mail.subject,
      template: mail.template,
      to: mail.to,
      attachments: attachments,
    };
    this.logger.log('Sending an email [' + mail.subject + ']');
    await this.mailerService.sendMail(mail_info);
  }

  async sendApplicationEmail(mail: MailDTO): Promise<void> {
    try {
      await this.sendEmail(mail);
      this.logger.log('Application email delivered successfully: ' + mail.to);
    } catch (error) {
      this.logger.error('Error sending application email: ' + mail.to, error);
      throw error;
    }
  }
}
