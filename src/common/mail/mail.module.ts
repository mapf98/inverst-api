import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MailService } from './mail.service';
import { LoggerModule } from '../logger/logger.module';
import { MailController } from './mail.controller';

@Module({
  imports: [
    LoggerModule,
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory(config: ConfigService) {
        return {
          pool: true,
          transport: {
            host: config.get('mail.host'),
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
              user: config.get('mail.user'),
              pass: config.get('mail.password'),
            },
            requireTLS: true,
            tls: {
              rejectUnauthorized: false,
            },
          },
          template: {
            dir: __dirname + '/templates',
            adapter: new HandlebarsAdapter(undefined, {
              inlineCssEnabled: true,
            }),
            options: {
              strict: true,
            },
          },
        };
      },
    }),
  ],
  controllers: [MailController],
  providers: [MailService, ConfigService],
  exports: [MailService],
})
export class MailModule {}
