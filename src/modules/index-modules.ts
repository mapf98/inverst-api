import { ClientProspectModule } from './client-prospect/client-prospect.module';
import { MailModule } from 'src/common/mail/mail.module';

export const indexModules = [ClientProspectModule, MailModule];
