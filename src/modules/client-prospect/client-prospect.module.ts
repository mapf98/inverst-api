import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'src/common/logger/logger.module';
import { ClientProspect } from './entities/client-prospect.entity';
import { ClientProspectController } from './client-prospect.controller';
import { ClientProspectService } from './client-prospect.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { Application } from './entities/application.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ClientProspect, Application]),
    LoggerModule,
    ClientsModule.register([
      {
        name: 'EMAIL_SERVICE',
        transport: Transport.RMQ,
        options: {
          queue: 'email_queue',
          queueOptions: {
            durable: true,
          },
        },
      },
    ]),
  ],
  controllers: [ClientProspectController],
  providers: [ClientProspectService, ConfigService],
})
export class ClientProspectModule {}
