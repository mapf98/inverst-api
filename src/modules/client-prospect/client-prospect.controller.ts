import {
  Body,
  Controller,
  Post,
  Get,
  Param,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ClientProspectService } from './client-prospect.service';
import {
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiTags,
  ApiProperty,
  ApiParam,
  ApiOkResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';

class ClientProspectBODY {
  @ApiProperty({
    description: 'Email of the client',
    example: 'example@email.com',
  })
  email: string;

  @ApiProperty({ description: 'Value of term1', example: 'Term 1 value' })
  term1: string;

  @ApiProperty({ description: 'Value of term2', example: 'Term 2 value' })
  term2: string;

  @ApiProperty({ description: 'Address of the client', example: '123 Main St' })
  address: string;

  @ApiProperty({ description: 'Apartment status', example: true })
  apt_status: boolean;

  @ApiProperty({ description: 'Seniority', example: 'Senior' })
  seniority: string;

  @ApiProperty({ description: 'Approximate seniority', example: '10 years' })
  seniority_aprox: string;

  @ApiProperty({ description: 'Square meters', example: 100 })
  mt2: number;

  @ApiProperty({ description: 'Approximate square meters', example: 90 })
  mt2_aprox: number;

  @ApiProperty({ description: 'Have gray work', example: true })
  have_gray_work: boolean;

  @ApiProperty({ description: 'Apartment type', example: 'Luxury' })
  apt_type: string;

  @ApiProperty({ description: 'Room bathroom count', example: 3 })
  room_bathroom_count: number;

  @ApiProperty({ description: 'Room without bathroom count', example: 2 })
  room_without_bathroom_count: number;

  @ApiProperty({ description: 'Total room count', example: 5 })
  room_count: number;

  @ApiProperty({ description: 'Have garage', example: true })
  have_garage: boolean;

  @ApiProperty({ description: 'Garage count', example: 2 })
  garage_count: number;

  @ApiProperty({ description: 'Garage type', example: 'Underground' })
  garage_type: string;

  @ApiProperty({ description: 'Apartment stratum', example: 5 })
  apt_stratum: number;

  @ApiProperty({ description: 'Additional garage', example: true })
  apt_additional_garage: boolean;

  @ApiProperty({ description: 'Additional laundry', example: true })
  apt_additional_laundry: boolean;

  @ApiProperty({ description: 'Additional communal living', example: true })
  apt_additional_communal_living: boolean;

  @ApiProperty({ description: 'Additional electric plant', example: true })
  apt_additional_electric_plant: boolean;

  @ApiProperty({ description: 'Additional study', example: true })
  apt_additional_study: boolean;

  @ApiProperty({ description: 'Additional green zone', example: true })
  apt_additional_green_zone: boolean;

  @ApiProperty({ description: 'Additional kids zone', example: true })
  apt_additional_kids_zone: boolean;

  @ApiProperty({ description: 'Additional BBQ zone', example: true })
  apt_additional_bbq_zone: boolean;

  @ApiProperty({ description: 'Additional gate', example: true })
  apt_additional_gate: boolean;

  @ApiProperty({ description: 'Additional humid zone', example: true })
  apt_additional_humid_zone: boolean;

  @ApiProperty({ description: 'Additional elevator', example: true })
  apt_additional_elevator: boolean;

  @ApiProperty({ description: 'Management value', example: 500 })
  management_value: number;

  @ApiProperty({ description: 'Apartment value', example: 100000 })
  apt_value: number;

  @ApiProperty({ description: 'Has assessment', example: true })
  has_assessment: boolean;

  @ApiProperty({ description: 'Assessment type', example: 'Bank' })
  assessment_type: string;

  @ApiProperty({ description: 'Assessment bank', example: 'ABC Bank' })
  assessment_bank: string;

  @ApiProperty({ description: 'Assessment value', example: 75000 })
  assessment_value: number;

  @ApiProperty({ description: 'Sell motivation', example: 'Relocation' })
  sell_motivation: string;

  @ApiProperty({ description: 'Is eligible', example: true })
  is_elegible: boolean;

  // Add more properties as needed
}

class ClientProspectAndFiles {
  @ApiProperty({
    description: 'Client prospect data',
    type: ClientProspectBODY,
  })
  clientProspect: ClientProspectBODY;

  @ApiProperty({
    description: 'Uploaded files',
    type: 'array',
    items: { type: 'string', format: 'binary' },
  })
  files: Express.Multer.File[];
}

class OTPData {
  @ApiProperty({
    description: 'Email of the client',
    example: 'example@email.com',
  })
  email: string;

  @ApiProperty({ description: 'Name of the client', example: 'John Doe' })
  name: string;

  @ApiProperty({
    description: 'Phone number of the client',
    example: '1234567890',
  })
  phone: string;

  @ApiProperty({
    description: 'Source through which the client was found',
    example: 'Advertisement',
  })
  found_throught: string;

  @ApiProperty({ description: 'Cookie policy acceptance', example: 'true' })
  cookiePolicyAccepted: string; // Boolean values are usually represented as strings in JSON
}

class ApplicationResponse {
  @ApiProperty({ description: 'ID of the application', example: '1' })
  id: string;

  @ApiProperty({
    description: 'Friendly ID of the application',
    example: 'xyz789',
  })
  friendlyID: string;

  @ApiProperty({ description: 'Value of term1', example: true })
  term1: boolean;

  @ApiProperty({ description: 'Value of term2', example: false })
  term2: boolean;

  @ApiProperty({
    description: 'Address of the application',
    example: '123 Main St',
  })
  address: string;

  @ApiProperty({ description: 'Status of the apartment', example: 'Vacant' })
  apt_status: string;

  @ApiProperty({ description: 'Seniority value', example: 3 })
  seniority: number;

  @ApiProperty({
    description: 'Whether seniority is approximate',
    example: true,
  })
  seniority_aprox: boolean;

  @ApiProperty({ description: 'Area in square meters', example: 120 })
  mt2: number;

  @ApiProperty({ description: 'Whether area is approximate', example: false })
  mt2_aprox: boolean;

  @ApiProperty({ description: 'Whether there is gray work', example: true })
  have_gray_work: boolean;

  @ApiProperty({ description: 'Type of the apartment', example: '2-bedroom' })
  apt_type: string;

  @ApiProperty({
    description: 'Count of rooms with attached bathrooms',
    example: 2,
  })
  room_bathroom_count: number;

  @ApiProperty({
    description: 'Count of rooms without attached bathrooms',
    example: 1,
  })
  room_without_bathroom_count: number;

  @ApiProperty({ description: 'Total room count', example: 3 })
  room_count: number;

  @ApiProperty({
    description: 'Whether the apartment has a garage',
    example: true,
  })
  have_garage: boolean;

  @ApiProperty({ description: 'Count of garage spaces', example: 2 })
  garage_count: number;

  @ApiProperty({ description: 'Type of the garage', example: 'Underground' })
  garage_type: string;

  @ApiProperty({ description: 'Stratum of the apartment', example: 5 })
  apt_stratum: number;

  @ApiProperty({
    description: 'Whether there is an additional garage',
    example: true,
  })
  apt_additional_garage: boolean;

  @ApiProperty({
    description: 'Whether there is additional laundry facility',
    example: true,
  })
  apt_additional_laundry: boolean;

  @ApiProperty({
    description: 'Whether there is additional communal living space',
    example: true,
  })
  apt_additional_communal_living: boolean;

  @ApiProperty({
    description: 'Whether there is an additional electric plant',
    example: true,
  })
  apt_additional_electric_plant: boolean;

  @ApiProperty({
    description: 'Whether there is an additional study room',
    example: true,
  })
  apt_additional_study: boolean;

  @ApiProperty({
    description: 'Whether there is an additional green zone',
    example: true,
  })
  apt_additional_green_zone: boolean;

  @ApiProperty({
    description: 'Whether there is an additional kids zone',
    example: true,
  })
  apt_additional_kids_zone: boolean;

  @ApiProperty({
    description: 'Whether there is an additional BBQ zone',
    example: true,
  })
  apt_additional_bbq_zone: boolean;

  @ApiProperty({
    description: 'Whether there is an additional gate',
    example: true,
  })
  apt_additional_gate: boolean;

  @ApiProperty({
    description: 'Whether there is an additional humid zone',
    example: true,
  })
  apt_additional_humid_zone: boolean;

  @ApiProperty({
    description: 'Whether there is an additional elevator',
    example: true,
  })
  apt_additional_elevator: boolean;

  @ApiProperty({ description: 'Management value', example: 500 })
  management_value: number;

  @ApiProperty({ description: 'Apartment value', example: 150000 })
  apt_value: number;

  @ApiProperty({
    description: 'Whether the application has an assessment',
    example: true,
  })
  has_assessment: boolean;

  @ApiProperty({ description: 'Type of the assessment', example: true })
  assessment_type: boolean;

  @ApiProperty({
    description: 'Bank associated with the assessment',
    example: 'ABC Bank',
  })
  assessment_bank: string;

  @ApiProperty({ description: 'Value of the assessment', example: 1000 })
  assessment_value: number;

  @ApiProperty({
    description: 'Motivation to sell',
    example: 'Moving to a new city',
  })
  sell_motivation: string;

  @ApiProperty({
    description: 'Value indicating if the application is eligible',
    example: true,
  })
  is_elegible: boolean;

  @ApiProperty({
    description: 'Join date of the application',
    example: '2023-08-25T10:00:00Z',
  })
  join_date: Date;
}

class ClientResponse {
  @ApiProperty({ description: 'ID of the client', example: '1' })
  id: string;

  @ApiProperty({ description: 'Friendly ID of the client', example: 'abc123' })
  friendlyID: string;

  @ApiProperty({
    description: 'Applications associated with the client',
    type: [ApplicationResponse],
  })
  applications: ApplicationResponse[];

  @ApiProperty({ description: 'Name of the client', example: 'John Doe' })
  name: string;

  @ApiProperty({
    description: 'Email of the client',
    example: 'john@example.com',
  })
  email: string;

  @ApiProperty({
    description: 'Phone number of the client',
    example: '123-456-7890',
  })
  phone: string;

  @ApiProperty({
    description: 'How the client found out about the service',
    example: 'Online Ad',
  })
  found_throught: string;

  @ApiProperty({ description: 'OTP code for verification', example: '123456' })
  otp_code: string;

  @ApiProperty({
    description: 'Join date of the client',
    example: '2023-08-25T10:00:00Z',
  })
  join_date: Date;

  @ApiProperty({
    description: 'Whether the cookie policy has been accepted',
    example: true,
  })
  cookie_policy_accepted: boolean;
}

@Controller('client-prospect')
@ApiTags('Client Prospect')
export class ClientProspectController {
  constructor(private readonly clientProspectService: ClientProspectService) {}

  @Post('/')
  @UseInterceptors(
    FilesInterceptor('files', 20, {
      storage: diskStorage({
        destination: './FILES',
        filename: function (req, file, callback) {
          callback(null, Date.now() + '_' + file.originalname);
        },
      }),
    }),
  )
  @ApiOperation({ summary: 'Add Client Prospect with Files' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: ClientProspectAndFiles })
  async addClientProspect(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() clientProspect: any,
  ): Promise<void> {
    await this.clientProspectService.addClientProspect(clientProspect, files);
  }

  @Post('/otp')
  @ApiOperation({ summary: 'Generate OTP' })
  @ApiBody({ description: 'Data for OTP generation', type: OTPData })
  async optGenerator(@Body() data): Promise<{ id: string }> {
    return await this.clientProspectService.optGenerator(data);
  }

  @Post('/otp-by-email')
  @ApiOperation({ summary: 'Generate OTP by Email' })
  @ApiBody({ description: 'Data for OTP generation by email', type: OTPData })
  async optGeneratorByEmail(@Body() data): Promise<{ id: string }> {
    return await this.clientProspectService.optGeneratorByEmail(data);
  }

  @Get('/otp/:email/validate/:opt')
  @ApiOperation({ summary: 'Validate OTP' })
  @ApiParam({
    name: 'email',
    description: 'Email of the client',
    example: 'example@email.com',
  })
  @ApiParam({ name: 'opt', description: 'OTP code', example: '123456' })
  async validateOpt(@Param() params): Promise<void> {
    await this.clientProspectService.validateOpt(params.email, params.opt);
  }

  @Get('/applications/:email')
  @ApiOperation({ summary: 'Get User Applications' })
  @ApiParam({
    name: 'email',
    description: 'Email of the client',
    example: 'example@email.com',
  })
  @ApiOkResponse({
    description: 'Successful response',
    type: ClientResponse,
  })
  @ApiBadRequestResponse({
    description: 'Incorrect OPT code or client not found',
  })
  async getUserApplications(@Param() params): Promise<any> {
    return await this.clientProspectService.getUserApplications(params.email);
  }
}
