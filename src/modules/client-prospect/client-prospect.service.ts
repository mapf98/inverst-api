import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { LoggerService } from 'src/common/logger/logger.service';
import { MailDTO } from 'src/common/mail/dto/mail.dto';
import { Repository } from 'typeorm';
import { ClientProspect } from './entities/client-prospect.entity';
import { ClientProxy } from '@nestjs/microservices';
import { Application } from './entities/application.entity';
import { nanoid } from 'nanoid';
import twilio = require('twilio');

@Injectable()
export class ClientProspectService {
  constructor(
    @Inject('EMAIL_SERVICE') private client: ClientProxy,
    @InjectRepository(ClientProspect)
    private clientProspectRepository: Repository<ClientProspect>,
    @InjectRepository(Application)
    private applicationRepository: Repository<Application>,
    private readonly configService: ConfigService,
    private readonly logger: LoggerService,
  ) {}

  private getTwilioClient() {
    return twilio(
      this.configService.get('api.twilioSID'),
      this.configService.get('api.twilioToken'),
    );
  }

  async addClientProspect(data: any, files: any): Promise<void> {
    try {
      const client = await this.clientProspectRepository.findOne({
        where: {
          email: data.email,
        },
      });

      const newApplication = {
        friendlyID: nanoid(7),
        client_prospect: client,
        term1: data.term1,
        term2: data.term2,
        address: data.address,
        apt_status: data.apt_status,
        seniority: data.seniority,
        seniority_aprox: data.seniority_aprox,
        mt2: data.mt2,
        mt2_aprox: data.mt2_aprox,
        have_gray_work: data.have_gray_work,
        apt_type: data.apt_type,
        room_bathroom_count: data.room_bathroom_count,
        room_without_bathroom_count: data.room_without_bathroom_count,
        room_count: data.room_count,
        have_garage: data.have_garage,
        garage_count: data.garage_count,
        garage_type: data.garage_type,
        apt_stratum: data.apt_stratum,
        apt_additional_garage: data.apt_additional_garage,
        apt_additional_laundry: data.apt_additional_laundry,
        apt_additional_communal_living: data.apt_additional_communal_living,
        apt_additional_electric_plant: data.apt_additional_electric_plant,
        apt_additional_study: data.apt_additional_study,
        apt_additional_green_zone: data.apt_additional_green_zone,
        apt_additional_kids_zone: data.apt_additional_kids_zone,
        apt_additional_bbq_zone: data.apt_additional_bbq_zone,
        apt_additional_gate: data.apt_additional_gate,
        apt_additional_humid_zone: data.apt_additional_humid_zone,
        apt_additional_elevator: data.apt_additional_elevator,
        management_value: data.management_value,
        apt_value: data.apt_value,
        has_assessment: data.has_assessment,
        assessment_type: data.assessment_type,
        assessment_bank: data.assessment_bank,
        assessment_value: data.assessment_value,
        sell_motivation: data.sell_motivation,
        is_elegible: data.is_elegible,
        join_date: new Date(),
      };

      Object.keys(newApplication).forEach((key) => {
        if (newApplication[key] == 'true') newApplication[key] = true;
        if (newApplication[key] == 'false') newApplication[key] = false;
      });

      const application = await this.applicationRepository.save(newApplication);
      const attachments = files.map((file) => {
        return { file: file.filename, name: file.originalname };
      });

      if (data.is_elegible === 'true') {
        const dataWithID = {
          ...data,
          applicationID: application.id,
          applicationFriendlyID: application.friendlyID,
          clientID: client.id,
          clientFriendlyID: client.friendlyID,
          join_date: new Date(),
        };

        // Admin email
        const adminEmail = new MailDTO(
          [this.configService.get('mail.defaultMail')],
          this.configService.get('mail.subject.application'),
          this.configService.get('mail.template.application'),
          dataWithID,
          attachments,
        );
        this.client.emit('application-email', adminEmail);

        // Client email
        const clientEmail = new MailDTO(
          [data.email],
          this.configService.get('mail.subject.application_for_client'),
          this.configService.get('mail.template.application_for_client'),
          {
            name: data.name,
            address: data.clientAddress,
            phone: data.phone,
            email: data.email,
            applicationID: application.id,
            applicationFriendlyID: application.friendlyID,
            clientID: client.id,
            clientFriendlyID: client.friendlyID,
          },
        );
        this.client.emit('application-email', clientEmail);
      } else {
        const dataWithID = {
          ...data,
          applicationID: application.id,
          applicationFriendlyID: application.friendlyID,
          clientID: client.id,
          clientFriendlyID: client.friendlyID,
          join_date: new Date(),
        };
        // Admin email
        const adminEmail = new MailDTO(
          [this.configService.get('mail.defaultMail')],
          this.configService.get('mail.subject.application_not_elegible'),
          this.configService.get('mail.template.application_not_elegible'),
          dataWithID,
          attachments,
        );
        this.client.emit('application-email', adminEmail);
      }

      this.logger.log('New clien prospect added: ' + data.email);
    } catch (error) {
      throw error;
    }
  }

  async optGenerator(data: any): Promise<{ id: string }> {
    let clientID = '';
    const opt = Math.floor(
      Math.random() * (900000 - 100000 + 1) + 100000,
    ).toString();
    const client = await this.clientProspectRepository.findOne({
      where: {
        email: data.email,
      },
    });

    if (client) {
      clientID = client.id;
      await this.clientProspectRepository.update(clientID, {
        otp_code: opt,
        cookie_policy_accepted: data.cookiePolicyAccepted === 'true' || false,
      });
      await this.getTwilioClient().messages.create({
        body: 'Bienvenido a INVERSTUCASA, tu código de verificación es: ' + opt,
        messagingServiceSid: 'MG8c25b7cf3baeba0bee2ec929c65fcf67',
        to: data.phone,
      });
    } else {
      const newClient = {
        friendlyID: nanoid(7),
        email: data.email,
        name: data.name,
        phone: data.phone,
        found_throught: data.found_throught,
        otp_code: opt,
        cookie_policy_accepted: data.cookiePolicyAccepted === 'true' || false,
      };

      const result = await this.clientProspectRepository.save(newClient);
      clientID = result.id;

      await this.getTwilioClient().messages.create({
        body: 'Bienvenido a INVERSTUCASA, su código de verificación es: ' + opt,
        messagingServiceSid: 'MG8c25b7cf3baeba0bee2ec929c65fcf67',
        to: data.phone,
      });
    }
    this.logger.log('New OPT code sended');
    return { id: clientID };
  }

  async optGeneratorByEmail(data: any): Promise<{ id: string }> {
    let clientID = '';
    const opt = Math.floor(
      Math.random() * (900000 - 100000 + 1) + 100000,
    ).toString();
    const client = await this.clientProspectRepository.findOne({
      where: {
        email: data.email,
      },
    });

    if (client) {
      clientID = client.id;
      await this.clientProspectRepository.update(clientID, {
        otp_code: opt,
        cookie_policy_accepted: data.cookiePolicyAccepted === 'true' || false,
      });
      const mail = new MailDTO(
        [data.email],
        this.configService.get('mail.subject.opt'),
        this.configService.get('mail.template.opt'),
        { opt: opt, id: clientID, friendlyID: client.friendlyID },
      );
      this.client.emit('application-email', mail);
    } else {
      const newClient = {
        friendlyID: nanoid(7),
        email: data.email,
        name: data.name,
        phone: data.phone,
        found_throught: data.found_throught,
        otp_code: opt,
        cookie_policy_accepted: data.cookiePolicyAccepted === 'true' || false,
      };

      const result = await this.clientProspectRepository.save(newClient);
      clientID = result.id;

      const mail = new MailDTO(
        [data.email],
        this.configService.get('mail.subject.opt'),
        this.configService.get('mail.template.opt'),
        { opt: opt, id: clientID, friendlyID: result.friendlyID },
      );
      this.client.emit('application-email', mail);
    }
    this.logger.log('New OPT code sended');
    return { id: clientID };
  }

  async validateOpt(email: string, opt: number): Promise<void> {
    const client = await this.clientProspectRepository.findOne({
      where: {
        email: email,
      },
    });

    if (client == null || client.otp_code !== opt.toString())
      throw new BadRequestException('Incorrect OPT code');
  }

  async getUserApplications(email: string): Promise<any> {
    const client = await this.clientProspectRepository.findOne({
      where: {
        email: email,
      },
    });

    if (client !== null) {
      const applications = await this.applicationRepository.find({
        where: {
          client_prospect: {
            id: client.id,
          },
          is_elegible: true,
        },
      });
      return { applications, client };
    }

    if (client == null) throw new BadRequestException('Incorrect OPT code');
  }
}
