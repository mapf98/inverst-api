import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';
import { Application } from './application.entity';
import { EncryptionTransformer } from 'typeorm-encrypted';
import { ecpConfig } from 'src/common/database/ecp-config';

@Entity()
export class ClientProspect extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    unique: true,
    nullable: false,
  })
  friendlyID: string;

  @OneToMany(() => Application, (application) => application.client_prospect)
  applications: Application[];

  @Column({
    type: 'varchar',
    unique: false,
    nullable: true,
    transformer: new EncryptionTransformer(ecpConfig),
  })
  name: string;

  @Column({
    unique: true,
    nullable: false,
  })
  email: string;

  @Column({
    type: 'varchar',
    unique: false,
    nullable: true,
    transformer: new EncryptionTransformer(ecpConfig),
  })
  phone: string;

  @Column({
    unique: false,
    nullable: true,
  })
  found_throught: string;

  @Column({
    type: 'varchar',
    unique: false,
    nullable: true,
    transformer: new EncryptionTransformer(ecpConfig),
  })
  otp_code: string;

  @Column({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
    nullable: false,
  })
  join_date: Date;

  @Column({
    unique: false,
    nullable: true,
    default: false,
  })
  cookie_policy_accepted: boolean;
}