import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { ClientProspect } from './client-prospect.entity';
import { EncryptionTransformer } from 'typeorm-encrypted';
import { ecpConfig } from 'src/common/database/ecp-config';

@Entity()
export class Application extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    unique: true,
    nullable: false,
  })
  friendlyID: string;

  // PASO 1
  @ManyToOne(() => ClientProspect, (client) => client.applications, {
    nullable: false,
  })
  @JoinColumn({ name: 'fk_client_prospect' })
  client_prospect: ClientProspect;

  @Column({
    unique: false,
    nullable: true,
  })
  term1: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  term2: boolean;

  // PASO 2
  @Column({
    type: 'varchar',
    unique: false,
    nullable: true,
    transformer: new EncryptionTransformer(ecpConfig),
  })
  address: string;

  // PASO 3
  @Column({
    unique: false,
    nullable: true,
  })
  apt_status: string;

  @Column({
    unique: false,
    nullable: true,
  })
  seniority: number;

  @Column({
    unique: false,
    nullable: true,
  })
  seniority_aprox: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  mt2: number;

  @Column({
    unique: false,
    nullable: true,
  })
  mt2_aprox: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  have_gray_work: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_type: string;

  @Column({
    unique: false,
    nullable: true,
  })
  room_bathroom_count: number;

  @Column({
    unique: false,
    nullable: true,
  })
  room_without_bathroom_count: number;

  @Column({
    unique: false,
    nullable: true,
  })
  room_count: number;

  @Column({
    unique: false,
    nullable: true,
  })
  have_garage: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  garage_count: number;

  @Column({
    unique: false,
    nullable: true,
  })
  garage_type: string;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_stratum: number;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_garage: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_laundry: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_communal_living: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_electric_plant: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_study: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_green_zone: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_kids_zone: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_bbq_zone: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_gate: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_humid_zone: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_additional_elevator: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  management_value: number;

  @Column({
    unique: false,
    nullable: true,
  })
  apt_value: number;

  @Column({
    unique: false,
    nullable: true,
  })
  has_assessment: boolean;

  @Column({
    unique: false,
    nullable: true,
  })
  assessment_type: boolean;

  @Column({
    type: 'varchar',
    unique: false,
    nullable: true,
    transformer: new EncryptionTransformer(ecpConfig),
  })
  assessment_bank: string;

  @Column({
    unique: false,
    nullable: true,
  })
  assessment_value: number;

  @Column({
    type: 'varchar',
    unique: false,
    nullable: true,
    transformer: new EncryptionTransformer(ecpConfig),
  })
  sell_motivation: string;

  // Elegible
  @Column({
    unique: false,
    nullable: true,
  })
  is_elegible: boolean;

  @Column({
    type: 'timestamptz',
    unique: false,
    nullable: false,
  })
  join_date: Date;
}
