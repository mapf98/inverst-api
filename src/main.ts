import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';
import { logger } from './common/middleware/logger.middleware';
import { ValidationPipe } from '@nestjs/common';
import helmet from 'helmet';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  const configService = app.get(ConfigService);

  const apiPrefix = configService.get('api.prefix');
  const apiPort = configService.get('api.port');

  app.setGlobalPrefix(apiPrefix);
  app.useGlobalPipes(new ValidationPipe());
  app.use(logger);
  app.use(helmet());

  if (configService.get('api.production')) {
    app.enableCors({
      origin: '*',
      methods: 'GET,PUT,POST,DELETE',
      preflightContinue: false,
      optionsSuccessStatus: 204,
    });
  }

  // RABBIT MQ MICROSERVICE
  const microservice = app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [configService.get('api.rmqHost')],
      queue: 'email_queue',
      queueOptions: {
        durable: true,
      },
    },
  });

  // Swagger Configuration
  const openApiConfiguration = new DocumentBuilder()
    .setTitle('Inverst Tu Casa')
    .setDescription('Inverst Tu Casa API backend functionality and use')
    .setVersion('1.0')
    .addTag('InverstTuCasa')
    .build();
  const document = SwaggerModule.createDocument(app, openApiConfiguration);
  SwaggerModule.setup('api', app, document);

  await app.startAllMicroservices();
  await app.listen(apiPort);
}
bootstrap();
